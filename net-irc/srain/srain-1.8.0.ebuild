# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit meson python-any-r1 xdg

DESCRIPTION="Modern, beautiful IRC client written in GTK+ 3"
HOMEPAGE="https://github.com/SrainApp/srain"
SRC_URI="https://github.com/SrainApp/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="doc libressl man test"

RDEPEND="
	app-crypt/libsecret
	>=dev-libs/glib-2.39.3
	>=dev-libs/libconfig-1.5
	!libressl? ( dev-libs/openssl )
	libressl? ( dev-libs/libressl )
	net-libs/libsoup
	>=x11-libs/gtk+-3.22.15
	dev-libs/libayatana-appindicator
"
DEPEND="${RDEPEND}"
BDEPEND="
	doc? (
		$(python_gen_any_dep '
        	dev-python/sphinx[${PYTHON_USEDEP}]
     	')
		${PYTHON_DEPS}
	)
	man? ( ${PYTHON_DEPS} )
"

python_check_deps() {
	if use doc || use man; then
		has_version -b "dev-python/sphinx[${PYTHON_USEDEP}]" || return 1
	fi
}

pkg_setup() {
	use test && python-any-r1_pkg_setup
}

src_prepare() {
	default

	sed "s/\('doc'\), meson.project_name()/\1, '${PF}'/" \
		-i meson.build || die
}

src_configure() {
	local -a doc_builders=()
	use doc && doc_builders+=( html )
	use man && doc_builders+=( man )

	local emesonargs=(
		-Ddoc_builders="$(meson-format-array "${doc_builders[@]}")"
	)
	meson_src_configure
}
