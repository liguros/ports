# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="HTTP load testing tool and library written in GoLang."
HOMEPAGE="https://github.com/tsenart/vegeta"
SRC_URI="
	https://github.com/tsenart/vegeta/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/farout/liguros-distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86 arm"

src_compile() {
	CGO_ENABLED=0 go build -v -a -tags=netgo -ldflags '-s -w -extldflags "-static"'
}

src_install() {
	# Install binary
	dobin ${PN}

	# Install docs
	einstalldocs
}
