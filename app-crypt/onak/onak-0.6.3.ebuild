# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools cmake

DESCRIPTION="onak is an OpenPGP keyserver"
HOMEPAGE="https://www.earth.li/projectpurple/progs/onak.html"
SRC_URI="https://github.com/u1f35c/onak/archive/refs/tags/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="berkdb postgres"

DEPEND="sys-libs/db:5.3
	postgres? ( dev-db/postgresql[server] )"

DOCS=(
	doc README.md LICENSE onak.sql
)

RESTRICT="test"
S="${WORKDIR}/${PN}-${P}"

src_prepare() {
	default
	cmake_src_prepare
}

src_configure() {
	local backend="fs"
	use berkdb && backend="db4"
	use postgres && backend="pg"
	if use berkdb && use postgres; then
		ewarn "berkdb and postgres requested, postgres was preferred"
	fi

	cmake_src_configure
}

src_install() {
	default
	insinto /etc
	newins onak.ini.in onak.ini
	keepdir /var/lib/onak
	dodir /usr/lib/cgi-bin/pks
	insinto /usr/lib/cgi-bin/pks
}
