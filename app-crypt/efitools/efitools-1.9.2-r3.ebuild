# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit flag-o-matic toolchain-funcs

DESCRIPTION="Tools for manipulating UEFI secure boot platforms"
HOMEPAGE="https://git.kernel.org/cgit/linux/kernel/git/jejb/efitools.git"
SRC_URI="https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools.git/snapshot/${P}.tar.gz"

LICENSE="GPL-2 LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm64 x86"
IUSE="libressl static"

LIB_DEPEND="
	!libressl? ( dev-libs/openssl:0=[static-libs(+)] )
	libressl? ( >=dev-libs/libressl-3.5.0:0=[static-libs(+)] )"

RDEPEND="
	!static? ( ${LIB_DEPEND//\[static-libs(+)]} )
	sys-apps/util-linux"

DEPEND="${RDEPEND}
	app-crypt/sbsigntools
	dev-perl/File-Slurp
	static? ( ${LIB_DEPEND} )
	sys-apps/help2man
	sys-boot/gnu-efi
	virtual/pkgconfig"

PATCHES=(
	"${FILESDIR}"/1.9.2-clang16.patch
	"${FILESDIR}"/1.9.2-Makefile.patch
)

src_prepare() {
	default

	# Let it build with clang
	if tc-is-clang; then
		sed -i -e 's/-fno-toplevel-reorder//g' Make.rules || die
	fi

	if use static; then
		append-ldflags -static
		export STATIC_FLAG=--static
	fi
}

src_configure() {
	# Calls LD directly, doesn't respect LDFLAGS. Low level package anyway.
	# See bug #908813.
	filter-lto

	tc-export AR CC LD NM OBJCOPY PKG_CONFIG
}
