# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit autotools

MY_PV=$(ver_rs 1-2 _)
DESCRIPTION="GTK+ frontend to the libexif library (parsing, editing, and saving EXIF data)"
HOMEPAGE="https://libexif.github.io/"
SRC_URI="https://github.com/libexif/${PN}/archive/${PN}-${MY_PV}-release.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha amd64 ~ia64 ppc sparc x86 ~amd64-linux ~x86-linux ~x86-solaris"
IUSE="nls"

RDEPEND="
	dev-libs/glib:2
	x11-libs/gtk+:2
	media-libs/libexif:="
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"
S="${WORKDIR}/${PN}-${PN}-${MY_PV}-release"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf \
		--disable-static \
		$(use_enable nls)
}

src_install() {
	default

	# no static archives
	find "${D}" -name '*.la' -delete || die
}
