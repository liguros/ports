# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="Calculate cyclomatic complexities of functions in Go source code"
HOMEPAGE="https://github.com/fzipp/gocyclo"

SRC_URI="
	https://github.com/fzipp/gocyclo/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 arm x86"

src_compile() {
	default
	go build -o ${PN} cmd/gocyclo/main.go
}

src_install() {
	dobin gocyclo
	default
}
