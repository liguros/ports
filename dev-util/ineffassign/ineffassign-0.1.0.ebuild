# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
KEYWORDS="~amd64"

inherit go-module bash-completion-r1

DESCRIPTION="Detects ineffectual assignments in Go code."
HOMEPAGE="https://github.com/gordonklaus/ineffassign"
SRC_URI="
	https://github.com/gordonklaus/ineffassign/archive/refs/tags/v${PV}.tar.gz
	https://gitlab.com/farout/liguros-distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="MIT License"
SLOT="0"
KEYWORDS="amd64 x86 arm"

src_compile() {
	cd ${S}
	mkdir -pv bin || die
	go build -o "${S}/bin/${PN}" || die
}

src_install() {
	dobin bin/*
	dodoc LICENSE README.md
}
