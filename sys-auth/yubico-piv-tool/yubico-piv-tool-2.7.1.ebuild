# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

DESCRIPTION="Command-line tool and p11-kit module for the YubiKey PIV application"
HOMEPAGE="https://developers.yubico.com/yubico-piv-tool/ https://github.com/Yubico/yubico-piv-tool"
SRC_URI="https://developers.yubico.com/yubico-piv-tool/Releases/${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0/2"
KEYWORDS="amd64"
IUSE="libressl test"

RESTRICT="!test? ( test )"

RDEPEND="
	sys-apps/pcsc-lite
	!libressl? ( dev-libs/openssl:=[-bindist(-)] )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )
	"
DEPEND="
	${RDEPEND}
	dev-libs/check"
BDEPEND="
	dev-util/gengetopt
	sys-apps/help2man
	virtual/pkgconfig"

PATCHES=(
	${FILESDIR}/yubico_piv_tool_c.patch
	${FILESDIR}/yubico-openssl-compat.patch
	"${FILESDIR}"/${PN}-2.1.1-tests-optional.patch
	"${FILESDIR}"/${PN}-2.1.1-ykcs11-threads.patch
	"${FILESDIR}"/${PN}-2.3.0-no-Werror.patch
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	# As of 2.2.0, man pages end up in /usr/usr/... without the MANDIR override
	local mycmakeargs=(
		-DBUILD_STATIC_LIB=OFF
		-DBUILD_TESTING=$(usex test)
		-DCMAKE_INSTALL_MANDIR="share/man"
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	echo "module: ${EPREFIX}/usr/$(get_libdir)/libykcs11.so" > ${PN}.module \
		|| die "Failed to generate p11-kit module configuration"
	insinto /usr/share/p11-kit/modules
	doins ${PN}.module
}
