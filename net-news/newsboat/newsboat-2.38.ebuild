# Copyright 2021-2023 Ligros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8


CRATES="
addr2line-0.24.1
adler2-2.0.0
aho-corasick-1.1.3
android-tzdata-0.1.1
android_system_properties-0.1.5
anstyle-1.0.10
ascii-canvas-3.0.0
assert-json-diff-2.0.2
async-attributes-1.1.2
async-channel-1.9.0
async-channel-2.3.1
async-executor-1.13.1
async-global-executor-2.4.1
async-io-2.3.4
async-lock-3.4.0
async-object-pool-0.1.5
async-process-2.3.0
async-signal-0.2.10
async-std-1.13.0
async-task-4.7.1
async-trait-0.1.82
atomic-waker-1.1.2
autocfg-1.3.0
backtrace-0.3.74
base64-0.21.7
basic-cookies-0.1.5
bit-set-0.5.3
bit-vec-0.6.3
bitflags-2.6.0
block-0.1.6
blocking-1.6.1
bumpalo-3.16.0
byteorder-1.5.0
bytes-1.7.2
cc-1.1.21
cfg-if-1.0.0
chrono-0.4.39
clap-4.5.21
clap_builder-4.5.21
clap_lex-0.7.3
codespan-reporting-0.11.1
concurrent-queue-2.5.0
core-foundation-sys-0.8.7
crossbeam-utils-0.8.20
crunchy-0.2.2
curl-sys-0.4.78+curl-8.11.0
cxx-1.0.135
cxx-build-1.0.135
cxxbridge-cmd-1.0.135
cxxbridge-flags-1.0.135
cxxbridge-macro-1.0.135
dirs-next-2.0.0
dirs-sys-next-0.1.2
displaydoc-0.2.5
either-1.13.0
ena-0.14.3
equivalent-1.0.1
errno-0.3.9
event-listener-2.5.3
event-listener-5.3.1
event-listener-strategy-0.5.2
fastrand-2.3.0
fixedbitset-0.4.2
fnv-1.0.7
foldhash-0.1.3
form_urlencoded-1.2.1
futures-channel-0.3.30
futures-core-0.3.30
futures-io-0.3.30
futures-lite-2.3.0
futures-macro-0.3.30
futures-task-0.3.30
futures-util-0.3.30
getrandom-0.2.15
gettext-rs-0.7.2
gettext-sys-0.21.4
gimli-0.31.0
gloo-timers-0.3.0
hashbrown-0.14.5
hermit-abi-0.3.9
hermit-abi-0.4.0
http-0.2.12
http-body-0.4.6
httparse-1.9.4
httpdate-1.0.3
httpmock-0.7.0
hyper-0.14.30
iana-time-zone-0.1.61
iana-time-zone-haiku-0.1.2
icu_collections-1.5.0
icu_locid-1.5.0
icu_locid_transform-1.5.0
icu_locid_transform_data-1.5.0
icu_normalizer-1.5.0
icu_normalizer_data-1.5.0
icu_properties-1.5.1
icu_properties_data-1.5.0
icu_provider-1.5.0
icu_provider_macros-1.5.0
idna-1.0.3
idna_adapter-1.2.0
indexmap-2.5.0
itertools-0.11.0
itoa-1.0.11
js-sys-0.3.70
kv-log-macro-1.0.7
lalrpop-0.20.2
lalrpop-util-0.20.2
lazy_static-1.5.0
levenshtein-1.0.5
lexopt-0.3.0
libc-0.2.169
libm-0.2.8
libredox-0.1.3
libz-sys-1.1.20
link-cplusplus-1.0.9
linux-raw-sys-0.4.14
litemap-0.7.3
locale_config-0.3.0
lock_api-0.4.12
log-0.4.22
malloc_buf-0.0.6
md5-0.7.0
memchr-2.7.4
minimal-lexical-0.2.1
miniz_oxide-0.8.0
mio-1.0.2
natord-1.0.9
new_debug_unreachable-1.0.6
nom-7.1.3
num-traits-0.2.19
objc-0.2.7
objc-foundation-0.1.1
objc_id-0.1.1
object-0.36.4
once_cell-1.19.0
parking-2.2.1
parking_lot-0.12.3
parking_lot_core-0.9.10
percent-encoding-2.3.1
petgraph-0.6.5
phf_shared-0.10.0
pico-args-0.5.0
pin-project-lite-0.2.14
pin-utils-0.1.0
piper-0.2.4
pkg-config-0.3.31
polling-3.7.3
ppv-lite86-0.2.20
precomputed-hash-0.1.1
proc-macro2-1.0.86
proptest-1.5.0
quick-error-1.2.3
quote-1.0.37
rand-0.8.5
rand_chacha-0.3.1
rand_core-0.6.4
rand_xorshift-0.3.0
redox_syscall-0.5.5
redox_users-0.4.6
regex-1.10.6
regex-automata-0.4.7
regex-syntax-0.8.4
rustc-demangle-0.1.24
rustix-0.38.41
rustversion-1.0.17
rusty-fork-0.3.0
ryu-1.0.18
same-file-1.0.6
scopeguard-1.2.0
scratch-1.0.7
section_testing-0.0.5
serde-1.0.210
serde_derive-1.0.210
serde_json-1.0.128
serde_regex-1.1.0
shlex-1.3.0
signal-hook-registry-1.4.2
similar-2.6.0
siphasher-0.3.11
slab-0.4.9
smallvec-1.13.2
socket2-0.5.7
stable_deref_trait-1.2.0
string_cache-0.8.7
strsim-0.11.1
syn-1.0.109
syn-2.0.77
synstructure-0.13.1
temp-dir-0.1.13
tempfile-3.14.0
term-0.7.0
termcolor-1.4.1
thiserror-1.0.64
thiserror-impl-1.0.64
tiny-keccak-2.0.2
tinystr-0.7.6
tokio-1.40.0
tokio-macros-2.4.0
tower-service-0.3.3
tracing-0.1.40
tracing-core-0.1.32
try-lock-0.2.5
unarray-0.1.4
unicode-ident-1.0.13
unicode-segmentation-1.12.0
unicode-width-0.1.14
unicode-width-0.2.0
unicode-xid-0.2.6
url-2.5.4
utf16_iter-1.0.5
utf8_iter-1.0.4
value-bag-1.9.0
vcpkg-0.2.15
wait-timeout-0.2.0
walkdir-2.5.0
want-0.3.1
wasi-0.11.0+wasi-snapshot-preview1
wasm-bindgen-0.2.93
wasm-bindgen-backend-0.2.93
wasm-bindgen-futures-0.4.43
wasm-bindgen-macro-0.2.93
wasm-bindgen-macro-support-0.2.93
wasm-bindgen-shared-0.2.93
web-sys-0.3.70
winapi-0.3.9
winapi-i686-pc-windows-gnu-0.4.0
winapi-util-0.1.9
winapi-x86_64-pc-windows-gnu-0.4.0
windows-core-0.52.0
windows-sys-0.52.0
windows-sys-0.59.0
windows-targets-0.52.6
windows_aarch64_gnullvm-0.52.6
windows_aarch64_msvc-0.52.6
windows_i686_gnu-0.52.6
windows_i686_gnullvm-0.52.6
windows_i686_msvc-0.52.6
windows_x86_64_gnu-0.52.6
windows_x86_64_gnullvm-0.52.6
windows_x86_64_msvc-0.52.6
write16-1.0.0
writeable-0.5.5
xdg-2.5.2
yoke-0.7.4
yoke-derive-0.7.4
zerocopy-0.7.35
zerocopy-derive-0.7.35
zerofrom-0.1.4
zerofrom-derive-0.1.4
zerovec-0.10.4
zerovec-derive-0.10.3
"


inherit cargo toolchain-funcs xdg-utils

DESCRIPTION="An RSS/Atom feed reader for text terminals"
HOMEPAGE="https://newsboat.org/ https://github.com/newsboat/newsboat"
SRC_URI="
	https://github.com/newsboat/newsboat/archive/refs/tags/r${PV}.tar.gz
	$(cargo_crate_uris ${CRATES})
"

LICENSE="Apache-2.0 BSD-2 Boost-1.0 CC0-1.0 ISC MIT Unlicense"
SLOT="0"
KEYWORDS="amd64 ~arm ~ppc64 x86"
IUSE="libressl"

RDEPEND="
	>=dev-db/sqlite-3.5:3
	>=dev-libs/stfl-0.21
	>=net-misc/curl-7.21.6
	>=dev-libs/json-c-0.11:=
	dev-libs/libxml2
	sys-libs/ncurses:0=[unicode(+)]
"
DEPEND="${RDEPEND}
	>=dev-ruby/asciidoctor-1.5.3
	virtual/pkgconfig
	sys-devel/gettext
	sys-libs/zlib
	!libressl? ( dev-libs/openssl )
	libressl? ( dev-libs/libressl )
"

S=${WORKDIR}/${PN}-r${PV}

src_configure() {
	./config.sh || die
}

src_compile() {
	export CARGO_HOME="${ECARGO_HOME}"
	emake prefix="/usr" CXX="$(tc-getCXX)" AR="$(tc-getAR)" RANLIB="$(tc-getRANLIB)"
}

src_test() {
	# tests require UTF-8 locale
	emake CXX="$(tc-getCXX)" AR="$(tc-getAR)" RANLIB="$(tc-getRANLIB)" test
	# Tests fail if in ${S} rather than in ${S}/test
	cd "${S}"/test || die
	./test || die
}

src_install() {
	emake DESTDIR="${D}" prefix="/usr" docdir="/usr/share/doc/${PF}" install
	einstalldocs
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
