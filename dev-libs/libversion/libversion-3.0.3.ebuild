# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

DESCRIPTION="Advanced version string comparison library"
HOMEPAGE="https://github.com/repology/libversion"
SLOT="0"

LICENSE="MIT"
DOCS=( README.md CHANGES.md )
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~mips ppc ppc64 ~riscv ~s390 sparc x86 ~amd64-linux ~x86-linux ~x64-macos ~x64-solaris"
SRC_URI="https://github.com/repology/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

src_prepare() {
	sed -i "s/LIBRARY DESTINATION lib/LIBRARY DESTINATION $(get_libdir)/" "${S}/libversion/CMakeLists.txt"
	sed -i "s/ARCHIVE DESTINATION lib/ARCHIVE DESTINATION $(get_libdir)/" "${S}/libversion/CMakeLists.txt"
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		"-DPKGCONFIGDIR=$(get_libdir)/pkgconfig"
	)
	cmake_src_configure
}
