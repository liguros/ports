# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="CTF library used by DTrace on Linux"
HOMEPAGE="https://github.com/oracle/libdtrace-ctf"
SRC_URI="https://github.com/oracle/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm64"
IUSE=""

DEPEND="
	${RDEPEND}
	virtual/os-headers
"
RDEPEND="
	dev-libs/elfutils
	sys-libs/zlib
"

#src_unpack() {
#	unpack ${A}
#}
