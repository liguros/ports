# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools multilib-minimal libtool verify-sig

DESCRIPTION="Free version of the SSL/TLS protocol forked from OpenSSL"
HOMEPAGE="https://www.libressl.org/"
SRC_URI="
	https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/${P}.tar.gz
	verify-sig? ( https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/${P}.tar.gz.asc )
"

LICENSE="ISC openssl"
# Reflects ABI of libcrypto.so and libssl.so. Since these can differ,
# we'll try to use the max of either. However, if either change between
# versions, we have to change the subslot to trigger rebuild of consumers.
SLOT="0/53"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~loong ~mips ~ppc ~ppc64 ~s390 ~sparc ~x86 ~amd64-linux ~ppc-macos ~x64-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE="+asm static-libs test"
RESTRICT="!test? ( test )"
REQUIRED_USE="test? ( static-libs )"

RDEPEND="!dev-libs/openssl"
DEPEND="${RDEPEND}"
PDEPEND="app-misc/ca-certificates"
BDEPEND="verify-sig? ( sec-keys/openpgp-keys-libressl )"

VERIFY_SIG_OPENPGP_KEY_PATH="${BROOT}"/usr/share/openpgp-keys/libressl.asc

MULTILIB_WRAPPED_HEADERS=( /usr/include/openssl/opensslconf.h )

PATCHES=(
	"${FILESDIR}"/${PN}-2.8.3-solaris10.patch
	"${FILESDIR}"/${PN}-3.7.2-genrsa-rand.patch
	"${FILESDIR}"/sha512.patch
)

src_prepare() {
	touch crypto/Makefile.in

	sed -i \
		-e '/^[ \t]*CFLAGS=/s#-g ##' \
		-e '/^[ \t]*CFLAGS=/s#-g"#"#' \
		-e '/^[ \t]*CFLAGS=/s#-O2 ##' \
		-e '/^[ \t]*CFLAGS=/s#-O2"#"#' \
		-e '/^[ \t]*USER_CFLAGS=/s#-O2 ##' \
		-e '/^[ \t]*USER_CFLAGS=/s#-O2"#"#' \
		configure || die "fixing CFLAGS failed"

	if ! use test ; then
	sed -i \
		-e '/^[ \t]*SUBDIRS =/s#tests##' \
		Makefile.in || die "Removing tests failed"
	fi

	eapply_user
	eautoreconf
}

multilib_src_configure() {
	local ECONF_SOURCE="${S}"
	local args=(
		$(use_enable asm)
		$(use_enable static-libs static)
		$(use_enable test tests)
	)
	econf "${args[@]}"
}

multilib_src_install_all() {
	einstalldocs
	find "${D}" -name '*.la' -exec rm -f {} + || die
}
