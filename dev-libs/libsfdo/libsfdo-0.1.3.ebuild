# Copyright 2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson-multilib

DESCRIPTION="A collection of libraries of the freedesktop.org specifications"
HOMEPAGE="https://gitlab.freedesktop.org/vyivel/libsfdo"
SRC_URI="https://gitlab.freedesktop.org/vyivel/libsfdo/-/archive/v0.1.3/${PN}-v${PV}.tar.gz"
S="${WORKDIR}/${PN}-v${PV}"

LICENSE="BSD 2-Clause Simplified License"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
#IUSE="7z cppparser +crypto +data examples +file2pagecompiler iodbc +json libressl mariadb +mongodb mysql +net odbc +pagecompiler pdf pocodoc sqlite +ssl test +util +xml +zip"
#RESTRICT="!test? ( test )"
#REQUIRED_USE="
#	7z? ( xml )
#	file2pagecompiler? ( pagecompiler )
#	iodbc? ( odbc )
#	mongodb? ( data )
#	mysql? ( data )
#	odbc? ( data )
#	pagecompiler? ( json net util xml )
#	pocodoc? ( cppparser util xml )
#	sqlite? ( data )
#	ssl? ( util )
#	test? ( data? ( sqlite ) json util xml )
#"

#BDEPEND="virtual/pkgconfig"
#RDEPEND="
#	>=dev-libs/libpcre-8.42
#	mysql? ( !mariadb? ( dev-db/mysql-connector-c:0= )
#	mariadb? ( dev-db/mariadb-connector-c:0= ) )
#	odbc? ( iodbc? ( dev-db/libiodbc )
#		!iodbc? ( dev-db/unixODBC ) )
#	sqlite? ( dev-db/sqlite:3 )
#	ssl? (
#		!libressl? ( dev-libs/openssl:0= )
#		libressl? ( >=dev-libs/libressl-3.6.0:0= )
#	)
#	xml? ( dev-libs/expat )
#	zip? ( sys-libs/zlib )
#"
#DEPEND="${RDEPEND}"
