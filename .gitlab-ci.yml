default:
  image: registry.gitlab.com/liguros/images/liguros/cicd

.select_vala: &valaSelect
  - emerge -qbkjuN dev-lang/vala app-eselect/eselect-vala
  - eselect vala list
  - eselect vala set 1

.job_template: &jobDef
  tags:
    - lxd
  # TODO: change emerge to use package binary cache but skip binary for the tested package
  stage: test
  only:
    refs:
      - merge_requests

.job_template_ope: &opeDef
  <<: *jobDef
  environment:
    name: qa-job

.job_template_lib: &libDef
  <<: *jobDef
  environment:
    name: qa-job-lib

.job_template_start: &jobStart
  - pwd
  - ego sync
  - ego profile f desktop

.job_template_end: &jobEnd
  - cp -r * /var/git/liguros-repo
  # Show git diff excluding deleted files
  - git diff --name-only --diff-filter=d "HEAD^..HEAD"
  - declare -a EBUILD
  - EBUILD=($(git diff --name-only --diff-filter=d "HEAD^..HEAD"|grep ebuild|awk -F "/" '{print $(NF-2)"/"$NF}' |rev | cut -f 2- -d '.'|rev))
  - for i in ${EBUILD[@]};
      do
        echo $i; emerge "=$i";
      done
  - echo "All ebuilds processed!"

before_script:
  - env-update && source /etc/profile
  # Setting up basic options
  - echo 'GENTOO_MIRRORS="http://mirrors.rit.edu/gentoo/ http://gentoo.c3sl.ufpr.br/ https://mirror.init7.net/gentoo/ https://mirror.netcologne.de/gentoo/ https://mirror.aarnet.edu.au/pub/gentoo/ https://ftp.jaist.ac.jp/pub/Linux/Gentoo/ https://mirror.csclub.uwaterloo.ca/gentoo-distfiles/"' >> /etc/portage/make.conf
  - echo 'FETCHCOMMAND="wget -t 2 -T 10 --passive-ftp -O \"\${DISTDIR}/\${FILE}\" \"\${URI}\""' >> /etc/portage/make.conf
  - echo 'RESUMECOMMAND="wget -c -t 2 -T 10 --passive-ftp -O \"\${DISTDIR}/\${FILE}\" \"\${URI}\""' >> /etc/portage/make.conf
  # Setting global emerge options so that we don't need to set them with every emerge
  - echo 'EMERGE_DEFAULT_OPTS="-q --autounmask=y --autounmask-unrestricted-atoms=y --autounmask-write=y --autounmask-continue=y --jobs=2"' >> /etc/portage/make.conf
  # mask dev-lang/rust in favor of dev-lang/rust-bin to speed up the jobs
  - echo 'dev-lang/rust' >> /etc/portage/package.mask
  # Add special USE flags
  - echo 'acct-user/git gitlab' >> /etc/portage/package.use
  - echo 'net-proxy/haproxy -slz' >> /etc/portage/package.use
  - echo 'www-client/palemoon -gtk2 gtk3' >> /etc/portage/package.use
  - echo 'media-sound/drumkv1 standalone' >> /etc/portage/package.use
  # Adding "uncritical" long compiling packages (like llvm) as binary, to speed up processing. Also some packages that have circular dependencies.
  # The rest should be emerged as source to enable autounmask
  - emerge -qkb --with-bdeps=y llvm-core/llvm media-libs/freetype media-libs/libsndfile sys-devel/clang net-libs/nodejs media-libs/mesa app-text/ghostscript-gpl sys-devel/gcc-config

stages:          # List of stages for jobs, and their order of execution
  - test

ebuild-test-fluxbox-slim:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m fluxbox
    - *valaSelect
    - *jobEnd

ebuild-test-fluxbox-slim-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m fluxbox
    - *valaSelect
    - *jobEnd

ebuild-test-xfce:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m xfce
    - *valaSelect
    - *jobEnd

ebuild-test-xfce-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m xfce
    - *valaSelect
    - *jobEnd

ebuild-test-lxde:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m lxde
    - *valaSelect
    - *jobEnd

ebuild-test-lxde-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m lxde
    - *valaSelect
    - *jobEnd

ebuild-test-cinnamon:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m cinnamon
    - *valaSelect
    - *jobEnd

ebuild-test-cinnamon-lib:
  <<: *libDef
  script:
    - *jobStart
    # remove ppp from network manger as it is not compatible with libressl
    - echo 'net-misc/networkmanager -ppp' >> /etc/portage/package.use
    - ego profile m cinnamon
    - *valaSelect
    - *jobEnd

ebuild-test-kde:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m kde
    - *valaSelect
    - *jobEnd

ebuild-test-kde-lib:
  <<: *libDef
  script:
    - *jobStart
    # remove ppp from network manger as it is not compatible with libressl
    - echo 'net-misc/networkmanager -ppp' >> /etc/portage/package.use
    - ego profile m kde
    - *valaSelect
    - *jobEnd

ebuild-test-lxqt:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m lxqt
    - *valaSelect
    - *jobEnd

ebuild-test-lxqt-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m lxqt
    - *valaSelect
    - *jobEnd

ebuild-test-mate:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m mate
    - *valaSelect
    - *jobEnd

ebuild-test-mate-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m mate
    - *valaSelect
    - *jobEnd

ebuild-test-gnome:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m gnome
    - *valaSelect
    - *jobEnd

ebuild-test-gnome-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m gnome
    - *valaSelect
    - *jobEnd

ebuild-test-steam:
  <<: *opeDef
  script:
    - *jobStart
    - ego profile m steam
    - *valaSelect
    - *jobEnd

ebuild-test-steam-lib:
  <<: *libDef
  script:
    - *jobStart
    - ego profile m steam
    - *valaSelect
    - *jobEnd
