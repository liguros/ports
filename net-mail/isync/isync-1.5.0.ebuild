# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="MailDir mailbox synchronizer"
HOMEPAGE="https://sourceforge.net/projects/isync/"
LICENSE="GPL-2"
SLOT="0"
SRC_URI="mirror://sourceforge/${PN}/${PN}/${PV}/${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~x86"
IUSE="libressl sasl ssl zlib"

RDEPEND="
	>=sys-libs/db-4.2:=
	sasl?	( dev-libs/cyrus-sasl )
	ssl?	(
			!libressl?	( >=dev-libs/openssl-0.9.6:0= )
			libressl?	( dev-libs/libressl:0= )
		)
	zlib?	( sys-libs/zlib:0= )
"
DEPEND=${RDEPEND}
BDEPEND="
	dev-lang/perl
"

src_prepare() {
	default
	[[ ${PV} == 9999 ]] && eautoreconf
}

src_configure() {
	econf \
		$(use_with ssl) \
		$(use_with sasl) \
		$(use_with zlib)
}
