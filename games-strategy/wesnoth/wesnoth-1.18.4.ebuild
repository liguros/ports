# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake flag-o-matic xdg

DESCRIPTION="Battle for Wesnoth - A fantasy turn-based strategy game"
HOMEPAGE="http://www.wesnoth.org https://github.com/wesnoth/wesnoth"
LUA_COMMIT="1ab3208a1fceb12fca8f24ba57d6e13c5bff15e3"
MARIADBPP_COMMIT="d4405b3f1f930cc4a617406c5e62ab1ae5026663"
SRC_URI="
	https://github.com/wesnoth/wesnoth/archive/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/viaduck/mariadbpp/archive/${MARIADBPP_COMMIT}.tar.gz -> ${PN}-${MARIADBPP_COMMIT}.tar.gz
	https://github.com/lua/lua/archive/${LUA_COMMIT}.tar.gz -> ${PN}-${LUA_COMMIT}.tar.gz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="dbus dedicated doc libressl nls server"

RDEPEND="
	acct-group/wesnoth
	acct-user/wesnoth
	>=dev-libs/boost-1.85.0:=[bzip2,context,icu,nls]
	>=media-libs/libsdl2-2.0.4:0[joystick,video,X]
	!dedicated? (
		dev-libs/glib:2
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
		>=media-libs/fontconfig-2.4.1
		>=media-libs/sdl2-image-2.0.0[jpeg,png]
		>=media-libs/sdl2-mixer-2.0.0[vorbis]
		media-libs/libvorbis
		>=x11-libs/pango-1.22.0
		>=x11-libs/cairo-1.10.0
		sys-libs/readline:0=
		dbus? ( sys-apps/dbus )
	)"
DEPEND="
	${RDEPEND}
	x11-libs/libX11
"
BDEPEND="
	sys-devel/gettext
	virtual/pkgconfig
"

PATCHES=(
	$FILESDIR/third_party-1.17.26.patch
)

src_prepare() {
	mv ${WORKDIR}/lua-${LUA_COMMIT}/* $S/src/modules/lua
	mv ${WORKDIR}/lua-${LUA_COMMIT}/.[a-z]* $S/src/modules/lua
	rmdir ${WORKDIR}/lua-${LUA_COMMIT}
	mv ${WORKDIR}/mariadbpp-${MARIADBPP_COMMIT}/* $S/src/modules/mariadbpp
	mv ${WORKDIR}/mariadbpp-${MARIADBPP_COMMIT}/.[a-z]* $S/src/modules/mariadbpp
	rmdir ${WORKDIR}/mariadbpp-${MARIADBPP_COMMIT}

	#if use libressl ; then
	#	eapply -p1 ${FILESDIR}/openssl.patch
	#fi

	cmake_src_prepare

	if ! use doc ; then
		sed -i \
			-e '/manual/d' \
			doc/CMakeLists.txt || die
	fi

	# respect LINGUAS (bug #483316)
	if [[ ${LINGUAS+set} ]] ; then
		local lang langs=()
		for lang in $(cat po/LINGUAS) ; do
			has ${lang} ${LINGUAS} && langs+=( ${lang} )
		done
		echo "${langs[@]}" > po/LINGUAS || die
	fi
}

src_configure() {
	filter-flags -ftracer -fomit-frame-pointer

	if use dedicated || use server ; then
		mycmakeargs=(
			-DENABLE_CAMPAIGN_SERVER="ON"
			-DENABLE_SERVER="ON"
			-DSERVER_UID="${PN}"
			-DSERVER_GID="${PN}"
			-DFIFO_DIR="/run/wesnothd"
			)
	else
		mycmakeargs=(
			-DENABLE_CAMPAIGN_SERVER="OFF"
			-DENABLE_SERVER="OFF"
			)
	fi
	mycmakeargs+=(
		-Wno-dev
		-DENABLE_GAME="$(usex !dedicated)"
		-DENABLE_DESKTOP_ENTRY="$(usex !dedicated)"
		-DENABLE_NLS="$(usex nls)"
		-DENABLE_NOTIFICATIONS="$(usex dbus)"
		-DENABLE_STRICT_COMPILATION="OFF"
		)
	cmake_src_configure
}

src_install() {
	local DOCS=( README.md changelog.md )
	cmake_src_install
	if use dedicated || use server ; then
		rmdir "${ED}"/run{/wesnothd,} || die
		newinitd "${FILESDIR}"/wesnothd.rc-r1 wesnothd
	fi
}
