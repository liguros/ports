# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools flag-o-matic systemd

DESCRIPTION="Utility for controlling IPMI enabled devices"
HOMEPAGE="https://github.com/ipmitool/ipmitool"
FNAME="IPMITOOL_1_8_19"
SRC_URI="https://github.com/ipmitool/ipmitool/archive/refs/tags/${FNAME}.tar.gz"
IUSE="libressl openbmc openipmi static"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~hppa ~ia64 ~ppc ~ppc64 ~x86"
LICENSE="BSD"

RDEPEND="
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
	openbmc? ( sys-apps/systemd:0= )
	sys-libs/readline:0="
DEPEND="${RDEPEND}
		>=dev-build/autoconf-2.69-r5
		openipmi? ( sys-libs/openipmi )
		virtual/os-headers"

PATCHES=(
	"${FILESDIR}/0001.0100-fix_buf_overflow.patch"
	"${FILESDIR}/0002.0500-fix_CVE-2011-4339.patch"
	"${FILESDIR}/0003.0600-manpage_longlines.patch"
	"${FILESDIR}/0006.0125-nvidia-iana.patch"
)

S=${WORKDIR}/${PN}-${FNAME}

src_prepare() {
	default

	# Gentoo chooses to install ipmitool in /usr/sbin
	# Where RedHat chooses /usr/bin
	sed -i -e \
		's,/usr/bin/ipmitool,/usr/sbin/ipmitool,g' \
		"${S}"/contrib/* \
		|| die "sed bindir failed"

	# Consistent RUNSTATEDIR
	sed -i -e \
		's,/var/run,/run,g' \
		"${S}"/contrib/* \
		"${S}"/lib/helper.c \
		"${S}"/src/ipmievd.c \
		|| die "sed /var/run failed"

	eautoreconf
}

src_configure() {
	# - LIPMI and BMC are the Solaris libs
	# - OpenIPMI is unconditionally enabled in the configure as there is compat
	# code that is used if the library itself is not available
	# FreeIPMI does build now, but is disabled until the other arches keyword it
	#	`use_enable freeipmi intf-free` \
	# --enable-ipmievd is now unconditional

	# for pidfiles, runstatedir not respected in all parts of code
	append-cppflags -D_PATH_VARRUN=/run/

	# WGET & CURL are set to avoid network interaction, we manually inject the
	# IANA enterprise-numbers file instead.
	#
	# DEFAULT_INTF=open # default to OpenIPMI, do not take external input
	WGET=/bin/true \
	CURL=/bin/true \
	DEFAULT_INTF=open \
	econf \
		$(use_enable static) \
		--enable-ipmishell \
		--enable-intf-lan \
		--enable-intf-usb \
		$(use_enable openbmc intf-dbus) \
		--enable-intf-lanplus \
		--enable-intf-open \
		--enable-intf-serial \
		--disable-intf-bmc \
		--disable-intf-dummy \
		--disable-intf-free \
		--disable-intf-imb \
		--disable-intf-lipmi \
		--disable-internal-md5 \
		--with-kerneldir=/usr \
		--bindir=/usr/sbin \
		--runstatedir=/run \
		CFLAGS="${CFLAGS}"

	# Fix linux/ipmi.h to compile properly. This is a hack since it doesn't
	# include the below file to define some things.
	echo "#include <asm/byteorder.h>" >>config.h
}

src_install() {
	emake DESTDIR="${D}" PACKAGE="${PF}" install
	rm -f "${D}"/usr/share/doc/${PF}/COPYING
	into /usr

	newinitd "${FILESDIR}"/${PN}-1.8.18-ipmievd.initd ipmievd
	newconfd "${FILESDIR}"/${PN}-1.8.18-ipmievd.confd ipmievd
	# From debian, less configurable than OpenRC
	systemd_dounit "${FILESDIR}"/ipmievd.service

	# Everything past this point is installing contrib/
	dosbin contrib/bmclanconf

	exeinto /usr/libexec
	doexe contrib/log_bmc.sh
	newinitd "${FILESDIR}/log_bmc-1.8.18.initd" log_bmc

	# contrib/exchange-bmc-os-info.init.redhat
	# contrib/exchange-bmc-os-info.service.redhat
	# contrib/exchange-bmc-os-info.sysconf
	exeinto /usr/libexec
	newexe contrib/exchange-bmc-os-info.init.redhat exchange-bmc-os-info
	insinto /etc/sysconfig
	newins contrib/exchange-bmc-os-info.sysconf exchange-bmc-os-info
	systemd_newunit contrib/exchange-bmc-os-info.service.redhat exchange-bmc-os-info.service
	newinitd "${FILESDIR}/exchange-bmc-os-info-1.8.18.initd" exchange-bmc-os-info

	# contrib/bmc-snmp-proxy
	# contrib/bmc-snmp-proxy.service
	# contrib/bmc-snmp-proxy.sysconf
	exeinto /usr/libexec
	doexe contrib/bmc-snmp-proxy
	insinto /etc/sysconfig
	newins contrib/bmc-snmp-proxy.sysconf bmc-snmp-proxy
	systemd_dounit contrib/bmc-snmp-proxy.service
	# TODO: initd for bmc-snmp-proxy

	insinto /usr/share/${PN}
	doins contrib/oem_ibm_sel_map

	docinto contrib
	cd "${S}"/contrib
	dodoc collect_data.sh create_rrds.sh create_webpage_compact.sh create_webpage.sh README
}
