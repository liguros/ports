# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

DESCRIPTION="Reliable PostgreSQL Backup & Restore"
HOMEPAGE="https://github.com/pgbackrest/pgbackrest"
SRC_URI="https://github.com/pgbackrest/pgbackrest/archive/refs/tags/release/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	>=dev-db/postgresql-13.8
	app-arch/zstd:=
	net-libs/libssh2
	dev-libs/libyaml
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}-release-${PV}"
