# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

CMAKE_MAKEFILE_GENERATOR=emake
inherit cmake

DESCRIPTION="MySQL database connector for C++ (mimics JDBC 4.0 API)"
HOMEPAGE="https://dev.mysql.com/downloads/connector/cpp/"
URI_DIR="Connector-C++"
SRC_URI="https://dev.mysql.com/get/Downloads/${URI_DIR}/${P}-src.tar.gz"

LICENSE="Artistic GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="legacy libressl"

RDEPEND="
	legacy? (
		dev-libs/boost:=
		virtual/mysql
	)
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.6.0:0= )"
DEPEND="${RDEPEND}"
S="${WORKDIR}/${P}-src"

src_configure() {
	local mycmakeargs=(
		-DWITH_JDBC=$(usex legacy)
	)

	cmake_src_configure
}
