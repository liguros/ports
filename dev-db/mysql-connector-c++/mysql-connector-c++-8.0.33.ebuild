# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

CMAKE_MAKEFILE_GENERATOR=emake
inherit cmake

DESCRIPTION="MySQL database connector for C++ (mimics JDBC 4.0 API)"
HOMEPAGE="https://dev.mysql.com/downloads/connector/cpp/"
URI_DIR="Connector-C++"
SRC_URI="https://dev.mysql.com/get/Downloads/${URI_DIR}/${P}-src.tar.gz"

LICENSE="Artistic GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="+legacy libressl"

RDEPEND="
	app-arch/lz4:=
	app-arch/zstd:=
	>=dev-libs/protobuf-3.19.6:=
	sys-libs/zlib
	legacy? (
		dev-libs/boost:=
		>=dev-db/mysql-connector-c-8.0.27:=
	)
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.6.0:0= )"
DEPEND="${RDEPEND}"
S="${WORKDIR}/${P}-src"

PATCHES=(
	"${FILESDIR}"/${PN}-8.0.30-fix-libressl-support.patch
	"${FILESDIR}"/${PN}-8.0.27-mysqlclient_r.patch
)

src_configure() {
	local mycmakeargs=(
		-DMAKE_ENABLE_C++17=ON
		-DBUNDLE_DEPENDENCIES=OFF
		-DWITH_PROTOBUF=system
		-DWITH_LZ4=system
		-DWITH_SSL=system
		-DWITH_ZLIB=system
		-DWITH_ZSTD=system
		-DWITH_JDBC=OFF
	)

	if use legacy ; then
		mycmakeargs+=(
			-DWITH_BOOST="${ESYSROOT}"/usr
			-DMYSQLCLIENT_STATIC_BINDING=0
			-DMYSQLCLIENT_STATIC_LINKING=0
		)
	fi

	cmake_src_configure
}
