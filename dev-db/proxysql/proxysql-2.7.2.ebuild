# Copyright 2023-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit flag-o-matic

DESCRIPTION="A high performance, high availability, protocol aware proxy for MySQL and forks"
HOMEPAGE="http://www.proxysql.com"
LICENSE="GPL-3"
SLOT="0"
SRC_URI="https://github.com/sysown/proxysql/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

KEYWORDS="~amd64"
#IUSE="threads"

RDEPEND="
	acct-user/proxysql
	acct-group/proxysql
	dev-db/sqlite
	dev-db/mariadb-connector-c
	dev-db/mysql-connector-c++
	dev-libs/libconfig
	dev-libs/libdaemon
	dev-libs/libev
	net-libs/libmicrohttpd
	dev-libs/libpcre
	dev-libs/re2
	net-misc/curl
	sys-libs/zlib
	>=virtual/mysql-5.0
"

DEPEND="${RDEPEND}"
BDEPEND="
	virtual/pkgconfig
"

src_prepare() {
	export GIT_VERSION=${PV}
	default
}

src_compile() {
	append-cxxflags -std=c++17 -fPIC -pthread -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -I.
	default
}

src_install(){
	insopts -m0755
	dobin src/proxysql

	insopts -m0600
	insinto /etc
	doins etc/proxysql.cnf

	insopts -m0755
	insinto /etc/init.d
	doins etc/init.d/proxysql
}
