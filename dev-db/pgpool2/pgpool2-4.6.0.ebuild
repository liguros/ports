# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

POSTGRES_COMPAT=( 13 14 15 16 17 )

inherit autotools postgres-multi

MY_P="${PN/2/-II}-${PV}"

DESCRIPTION="Connection pool server for PostgreSQL"
HOMEPAGE="https://www.pgpool.net/"
SRC_URI="https://www.pgpool.net/download.php?f=${MY_P}.tar.gz -> ${MY_P}.tar.gz"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc libressl memcached pam ssl static-libs"

RDEPEND="
	${POSTGRES_DEP}
	net-libs/libnsl:0=
	memcached? ( dev-libs/libmemcached )
	pam? ( sys-auth/pambase )
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( >=dev-libs/libressl-3.5.0:= )
	)
	acct-user/pgpool
	virtual/libcrypt
"
DEPEND="
	${RDEPEND}
	sys-devel/bison
	virtual/pkgconfig
"

S=${WORKDIR}/${MY_P}

pkg_setup() {
	postgres-multi_pkg_setup
}

src_prepare() {
	eapply \
		"${FILESDIR}/pgpool-4.2.0-configure-memcached.patch" \
		"${FILESDIR}/pgpool-configure-pam.patch" \
		"${FILESDIR}/pgpool-4.2.0-configure-pthread.patch" \
		"${FILESDIR}/pgpool2-4.3.1-ssl_utils.patch"

	eautoreconf

	postgres-multi_src_prepare
}

src_configure() {
	postgres-multi_foreach econf \
		--disable-rpath \
		--sysconfdir="${EROOT}/etc/${PN}" \
		--with-pgsql-includedir='/usr/include/postgresql-@PG_SLOT@' \
		--with-pgsql-libdir="/usr/$(get_libdir)/postgresql-@PG_SLOT@/$(get_libdir)" \
		$(use_enable static-libs static) \
		$(use_with memcached) \
		$(use_with pam) \
		$(use_with ssl openssl)
}

src_compile() {
	# Even though we're only going to do an install for the best slot
	# available, the extension bits in src/sql need some things outside
	# of that directory built, too.
	postgres-multi_foreach emake
	postgres-multi_foreach emake -C src/sql
}

src_install() {
	# We only need the best stuff installed
	postgres-multi_forbest emake DESTDIR="${D}" install

	# Except for the extension and .so files that each PostgreSQL slot needs
	postgres-multi_foreach emake DESTDIR="${D}" -C src/sql install

	newinitd "${FILESDIR}/${PN}.initd" ${PN}
	newconfd "${FILESDIR}/${PN}.confd" ${PN}

	# Documentation!
	dodoc NEWS TODO
	doman doc/src/sgml/man{1,8}/*
	use doc && dodoc -r doc/src/sgml/html

	# mv some files that get installed to /usr/share/pgpool-II so that
	# they all wind up in the same place
	mv "${ED}/usr/share/${PN/2/-II}" "${ED}/usr/share/${PN}" || die

	# One more thing: Evil la files!
	find "${ED}" -name '*.la' -exec rm -f {} +
}
