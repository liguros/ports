# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="PostgreSQL extension with support for version string comparison"
HOMEPAGE="https://github.com/repology/postgresql-libversion"
SRC_URI="https://github.com/repology/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
SLOT="0"
KEYWORDS="amd64 ~hppa ~ppc ppc64 x86"

DEPEND="
	dev-python/libversion
	dev-db/postgresql
	dev-util/pkgconf
"

src_prepare() {
	default
}

src_compile() {
	make USE_PGXS=1
}
