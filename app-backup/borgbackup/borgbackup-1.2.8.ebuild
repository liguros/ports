# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
DISTUTILS_USE_PEP517=setuptools

inherit bash-completion-r1 distutils-r1

DESCRIPTION="Deduplicating backup program with compression and authenticated encryption"
HOMEPAGE="https://borgbackup.readthedocs.io/ https://pypi.org/project/borgbackup/"
SRC_URI="https://files.pythonhosted.org/packages/93/87/98299ebfe41687f77ea01bd0e9eba2f43baa30f1b9256345134fd77286d3/borgbackup-1.2.8.tar.gz"

KEYWORDS="amd64 ~arm ~arm64 ~ppc64 ~riscv x86"
LICENSE="BSD"
SLOT="0"
IUSE="libressl"

RDEPEND="
	!!app-office/borg
	app-arch/lz4
	virtual/acl
	dev-python/pyfuse3[${PYTHON_USEDEP}]
	dev-python/msgpack[${PYTHON_USEDEP}]
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
"

DEPEND="
	dev-python/setuptools-scm[${PYTHON_USEDEP}]
	dev-python/packaging[${PYTHON_USEDEP}]
	>=dev-python/cython-0.29.29[${PYTHON_USEDEP}]
	dev-python/pkgconfig[${PYTHON_USEDEP}]
	dev-python/tox
	dev-python/pytest-xdist
	dev-python/pytest-benchmark
	dev-python/twine
	${RDEPEND}
"

src_install() {
	distutils-r1_src_install
	doman docs/man/*

	dobashcomp scripts/shell_completions/bash/borg

	insinto /usr/share/zsh/site-functions
	doins scripts/shell_completions/zsh/_borg

	insinto /usr/share/fish/vendor_completions.d
	doins scripts/shell_completions/fish/borg.fish
}
