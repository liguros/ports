# Copyright 2020-2025 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=hatchling
PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit distutils-r1 pypi

DESCRIPTION="Generate distribution packages from PyPI"
HOMEPAGE="https://github.com/openSUSE/py2pack https://pypi.org/project/py2pack/"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND=""

DEPEND="${RDEPEND}
	dev-python/jinja2
	dev-python/six
	dev-python/metaextract
	dev-python/pbr
	dev-python/setuptools[${PYTHON_USEDEP}]
	"
