# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11} )

inherit distutils-r1

MY_PN=${PN/-/_}
DESCRIPTION="Client-Server SDK for Matrix"
HOMEPAGE="https://github.com/matrix-org/matrix-python-sdk https://pypi.org/project/matrix-client/"
#SRC_URI="mirror://pypi/${P:0:1}/${PN}/${MY_PN}-${PV}.tar.gz -> ${P}.tar.gz"
SRC_URI="https://files.pythonhosted.org/packages/93/9a/23c4894da5aeb316903677c71014575c486616026b85e2d0b408f84d8540/matrix_client-0.4.0.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86 ~amd64-linux ~x86-linux"
IUSE="test"

RDEPEND=">=dev-python/requests-2.22[${PYTHON_USEDEP}]
	dev-python/urllib3[${PYTHON_USEDEP}]"
DEPEND="${REDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	test? (
		dev-python/nose[${PYTHON_USEDEP}]
		dev-python/pytest[${PYTHON_USEDEP}]
	)"

S="${WORKDIR}/${MY_PN}-${PV}"

src_prepare() {
	cd ${S}
	eapply ${FILESDIR}/setup_py.patch
	# removing test directory, as it is not allowed to be installed
	rm -rf test
	default
}

python_test() {
	nosetests --verbose || die
	py.test -v -v || die
}
