# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_EXT=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12,13} )
PYPI_NO_NORMALIZE=1
PYPI_PN="M2Crypto"
PYTHON_REQ_USE="threads(+)"

inherit distutils-r1 toolchain-funcs

DESCRIPTION="A Python crypto and SSL toolkit"
HOMEPAGE="https://gitlab.com/m2crypto/m2crypto https://pypi.org/project/M2Crypto/"
SRC_URI="https://gitlab.com/m2crypto/m2crypto/-/archive/${PV}/m2crypto-${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~loong ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86 ~amd64-linux ~x86-linux ~x64-macos"
IUSE="libressl"

BDEPEND="
	>=dev-lang/swig-2.0.9
"
RDEPEND="
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )
"
DEPEND="${RDEPEND}"

#PATCHES=(
#	${FILESDIR}/${PN}-libressl-0.38.0-r2.patch
#)

swig_define() {
	local x
	for x; do
		if tc-cpp-is-true "defined(${x})"; then
			SWIG_FEATURES+=" -D${x}"
		fi
	done
}

src_prepare() {
	if use libressl; then
		eapply -p0 ${FILESDIR}/patch-src_SWIG__x509_i
		eapply -p0 ${FILESDIR}/patch-src_SWIG__lib11_compat_i
		eapply -p0 ${FILESDIR}/patch-src_SWIG__lib_i
		eapply -p0 ${FILESDIR}/patch-src_SWIG__threads_i
	fi

	sed -e 's:test_server_simple_timeouts:_&:' \
		-i tests/test_ssl.py || die
	distutils-r1_src_prepare
}

python_compile() {
	# setup.py looks at platform.machine() to determine swig options.
	# For exotic ABIs, we need to give swig a hint.
	local -x SWIG_FEATURES=

	# https://bugs.gentoo.org/617946
	swig_define __ILP32__
	# https://bugs.gentoo.org/674112
	swig_define __ARM_PCS_VFP

	distutils-r1_python_compile
}

python_test() {
	"${EPYTHON}" -m unittest -b -v tests.alltests.suite ||
		die "Tests failed for ${EPYTHON}"
}
