# Copyright 2022-2025 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_EXT=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit distutils-r1 multiprocessing prefix pypi

DESCRIPTION="High-performance RPC framework (python libraries)"
HOMEPAGE="https://grpc.io https://pypi.org/project/grpcio/"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~arm arm64 ~ppc64 ~riscv x86"
IUSE="libressl"

RDEPEND="
	!libressl? ( >=dev-libs/openssl-1.1.1:0=[-bindist(-)] )
	>=dev-libs/re2-0.2021.11.01:=
	dev-python/cython[${PYTHON_USEDEP}]
	>=dev-libs/protobuf-25.0
	dev-python/protobuf[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
	net-dns/c-ares:=
	sys-libs/zlib:=
"

DEPEND="${RDEPEND}"

src_prepare() {
	default
}

python_prepare_all() {
	distutils-r1_python_prepare_all
	hprefixify setup.py
}

python_configure_all() {
	if use libressl; then
		export GRPC_BUILD_WITH_BORING_SSL_ASM=1
		export GRPC_PYTHON_BUILD_SYSTEM_OPENSSL=0
	else
		export GRPC_BUILD_WITH_BORING_SSL_ASM=9
		export GRPC_PYTHON_BUILD_SYSTEM_OPENSSL=1
	fi

	export GRPC_PYTHON_DISABLE_LIBC_COMPATIBILITY=1
	export GRPC_PYTHON_BUILD_SYSTEM_CARES=1
	export GRPC_PYTHON_BUILD_WITH_SYSTEM_RE2=1
	export GRPC_PYTHON_BUILD_SYSTEM_ZLIB=1
	export GRPC_PYTHON_BUILD_WITH_CYTHON=1
	export GRPC_PYTHON_BUILD_EXT_COMPILER_JOBS="$(makeopts_jobs)"
}
