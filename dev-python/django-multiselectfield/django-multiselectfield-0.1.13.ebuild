# Copyright 2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12,13} )

inherit distutils-r1

GIT_COMMIT="d78d65219ec600a4078ad2b7d69e7af28c2681a3"

DESCRIPTION="Django multiple select field"
HOMEPAGE="https://github.com/goinnn/django-multiselectfield"
SRC_URI="https://github.com/goinnn/django-multiselectfield/archive/${GIT_COMMIT}.tar.gz -> ${P}.gh.tar.gz"

S=${WORKDIR}/${PN}-${GIT_COMMIT}

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="dev-python/django[${PYTHON_USEDEP}]"
DEPEND="${RDEPEND}"

DOCS=( README.rst )
