# Copyright 2024-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9,10,11,12,13} )
COMMIT="7fd3100cfb34b1d6d6c5815e06cf9aa2e9c6be53"

inherit distutils-r1

DESCRIPTION="Soft delete models, managers, queryset for Django"
HOMEPAGE="https://github.com/san4ezy/django_softdelete"
SRC_URI="https://github.com/san4ezy/django_softdelete/archive/${COMMIT}.tar.gz -> ${P}.gh.tar.gz"
S="${WORKDIR}/django_softdelete-${COMMIT}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="dev-python/django[${PYTHON_USEDEP}]"
BDEPEND="
	test? (
		>=dev-python/pytest-django-4.5.2[${PYTHON_USEDEP}]
	)
"

distutils_enable_tests pytest
