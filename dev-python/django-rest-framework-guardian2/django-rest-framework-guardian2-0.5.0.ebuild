# Copyright 2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8,9,10,11} )

inherit distutils-r1

DESCRIPTION="A Django application to retrieve user's IP address"
HOMEPAGE="
	https://github.com/johnthagen/django-rest-framework-guardian2
	https://pypi.org/project/djangorestframework-guardian2/
"
SRC_URI="https://github.com/johnthagen/django-rest-framework-guardian2/archive/refs/tags/v${PV}.tar.gz -> ${P}.gh.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND=">=dev-python/django-1.11[${PYTHON_USEDEP}]"

python_test() {
	"${EPYTHON}" tests/manage.py test -v2 myapp || die "Tests failed with ${EPYTHON}"
}
