# Copyright 2020-2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
PYTHON_COMPAT=( python3_{6,7,8,9,10,11} )

inherit distutils-r1

DESCRIPTION="Another Portage Python Interface"
HOMEPAGE="https://pypi.org/project/appi/"
SRC_URI="https://files.pythonhosted.org/packages/80/db/7d6b5559ac5c9ce0ab453666dabd633b1b294e41f147cd270a271c2c8f81/appi-0.2.5.tar.gz"

LICENSE="GPL-2"
SLOT="0/0"
KEYWORDS="*"
IUSE=""
