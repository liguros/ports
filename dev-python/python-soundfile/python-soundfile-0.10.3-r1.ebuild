# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
inherit distutils-r1

DESCRIPTION="SoundFile is an audio library based on libsndfile, CFFI and NumPy."
HOMEPAGE="https://github.com/bastibe/python-soundfile"
SRC_URI="${HOMEPAGE}/archive/refs/tags/${PV}post1.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-python/cffi
	dev-python/numpy
	media-libs/libsndfile"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}post1"
