# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10,11} )

DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="stream JSON parser"
HOMEPAGE="https://github.com/AMDmi3/jsonslicer"
SRC_URI="https://github.com/AMDmi3/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

SLOT="0"
LICENSE="MIT"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux ~amd64-fbsd"

DEPEND="
	dev-util/pkgconf
	dev-libs/yajl
"
