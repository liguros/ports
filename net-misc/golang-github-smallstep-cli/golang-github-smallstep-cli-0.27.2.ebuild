# Copyright 2023-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="zero trust swiss army knife for working with X509, OAuth, JWT, OATH OTP, etc".
HOMEPAGE="https://smallstep.com/cli/index.html"
SRC_URI="
	https://github.com/smallstep/cli/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="test"
DEPEND=""
RDEPEND=""
RESTRICT="!test? ( test )"

S=${WORKDIR}/cli-${PV}

src_unpack() {
	default
}

src_compile() {
	env GOBIN="${S}/bin" go install ./... ||
		die "compile failed"
}

src_install() {
	dobin bin/*
}
