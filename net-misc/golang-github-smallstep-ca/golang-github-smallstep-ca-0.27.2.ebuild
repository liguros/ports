# Copyright 2023-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="A private certificate authority (X.509 & SSH) & ACME server"
HOMEPAGE="https://github.com/smallstep/certificates"
SRC_URI="
	https://github.com/smallstep/certificates/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="test"
DEPEND=""
RDEPEND=""
RESTRICT="!test? ( test )"

S=${WORKDIR}/certificates-${PV}

src_unpack() {
	default
}

src_compile() {
	env GOBIN="${S}/bin" go install ./... ||
		die "compile failed"
}

src_install() {
	dobin bin/*
}
