# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

DESCRIPTION="extra plugins for wayfire"
HOMEPAGE="https://github.com/WayfireWM/wayfire-plugins-extra"
SRC_URI="https://github.com/WayfireWM/wayfire-plugins-extra/releases/download/v${PV}/${P}.tar.xz"
KEYWORDS="amd64 ~arm64"

LICENSE="MIT"
SLOT="0/$(ver_cut 1-2)"

# no tests
RESTRICT="test"

WAYFIRE_REVDEP="
	dev-libs/glib:2
	dev-libs/libsigc++:2
	gui-libs/wlroots:=
	x11-libs/cairo
"

DEPEND="
	${WAYFIRE_REVDEP}
	dev-cpp/glibmm:2
	dev-cpp/nlohmann_json
	dev-libs/libevdev
	dev-libs/wayland
	>=gui-wm/wayfire-0.8.1
"
RDEPEND="${DEPEND}"
BDEPEND="
	>=dev-libs/wayland-protocols-1.12
	dev-util/wayland-scanner
	virtual/pkgconfig
"
