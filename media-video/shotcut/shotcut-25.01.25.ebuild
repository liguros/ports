# Copyright 2021-2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake flag-o-matic xdg-utils

DESCRIPTION="A free, open source, cross-platform video editor"
HOMEPAGE="https://www.shotcut.org/ https://github.com/mltframework/shotcut/"
MY_PV="${PV}"
SRC_URI="https://github.com/mltframework/shotcut/archive/refs/tags/v${MY_PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"
IUSE="debug"
LICENSE="GPL-3+"
SLOT="0"
S=${WORKDIR}/${PN}-${MY_PV}

COMMON_DEPEND="
	dev-qt/qtbase:6[opengl(+)]
	dev-qt/qtdeclarative:6[opengl(+)]
	dev-qt/qtmultimedia:6
	dev-qt/qttools:6
	dev-qt/qtcharts:6
	media-video/ffmpeg
	>=media-libs/mlt-7.22
	sci-libs/fftw
"
DEPEND="${COMMON_DEPEND}
"
RDEPEND="${COMMON_DEPEND}
	virtual/jack
"

src_configure() {
	CMAKE_BUILD_TYPE=$(usex debug Debug Release)
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX=/usr
		-DSHOTCUT_VERSION="${PV}"
	)
	use debug || append-cxxflags "-DNDEBUG"
	append-cxxflags "-DSHOTCUT_NOUPGRADE"
	cmake_src_configure
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
	xdg_icon_cache_update
}
