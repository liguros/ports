# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="a simple video player that is synchronized to jack transport."
HOMEPAGE="https://xjadeo.sourceforge.net/"
SRC_URI="https://github.com/x42/xjadeo/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="alsa imlib osc osd sdl tools xv"

RDEPEND="virtual/jack
	media-video/ffmpeg:=
	alsa? ( >=media-libs/alsa-lib-1.0.10 )
	imlib? ( >=media-libs/imlib2-1.3.0 )
	osc? ( media-libs/liblo )
	sdl? ( >=media-libs/libsdl-1.2.8 )"

DEPEND="${RDPEND}
	>=sys-libs/zlib-1.2.2
	x11-misc/xcb
	virtual/pkgconfig"

DOCS=( AUTHORS ChangeLog README NEWS )

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	local myeconfargs=(
		$(use_enable imlib imlib2)
		$(use_enable osc)
		$(use_enable osd ft)
		$(use_enable sdl)
		$(use_enable tools contrib)
		$(use_enable xv)
		--disable-qtgui
		--disable-timescale
		--disable-portmidi
		--enable-silent-rules
	)

	econf "${myeconfargs[@]}"
}

src_install() {
	default

	if use tools; then
		newdoc contrib/cli-remote/README README.cli-remote
		dobin contrib/cli-remote/jadeo-rcli
		insinto "/usr/share/${PN}"
		doins "contrib/${PN}-example.mp4"
	fi
}
