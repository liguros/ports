# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit toolchain-funcs xdg

DESCRIPTION="Implementation of the MPEG-4 Systems standard developed from scratch in ANSI C"
HOMEPAGE="https://gpac.wp.imt.fr/"
GIT_COMMIT="5d70253ac94e5840be7b86054131dd753af63cc7"
SRC_URI="https://github.com/gpac/gpac/archive/${GIT_COMMIT}.tar.gz -> ${PN}-${GIT_COMMIT}.tar.gz"
KEYWORDS="~alpha ~amd64 ~ppc ~ppc64 ~sparc ~x86"

LICENSE="GPL-2"
# subslot == libgpac major
SLOT="0/12"
IUSE="a52 aac alsa cpu_flags_x86_sse2 dvb ffmpeg jack jpeg jpeg2k mad opengl oss png pulseaudio theora truetype vorbis xml xvid X"

BDEPEND="virtual/pkgconfig"
RDEPEND="
	media-libs/libogg
	sys-libs/zlib
	a52? ( media-libs/a52dec )
	aac? ( media-libs/faad2 )
	alsa? ( media-libs/alsa-lib )
	ffmpeg? ( media-video/ffmpeg:0= )
	jack? ( virtual/jack )
	jpeg? ( media-libs/libjpeg-turbo:0= )
	jpeg2k? ( media-libs/openjpeg:2 )
	mad? ( media-libs/libmad )
	opengl? (
		media-libs/freeglut
		virtual/glu
		virtual/opengl
	)
	png? ( media-libs/libpng:0= )
	pulseaudio? ( media-libs/libpulse )
	theora? ( media-libs/libtheora )
	truetype? ( media-libs/freetype:2 )
	vorbis? ( media-libs/libvorbis )
	X? (
		x11-libs/libXt
		x11-libs/libX11
		x11-libs/libXv
		x11-libs/libXext
	)
	xml? ( dev-libs/libxml2:2= )
	xvid? ( media-libs/xvid )
"
DEPEND="
	${RDEPEND}
	dvb? ( sys-kernel/linux-headers )
"

S=${WORKDIR}/${PN}-${GIT_COMMIT}

DOCS=(
	share/doc/CODING_STYLE
	share/doc/GPAC\ UPnP.doc
	share/doc/ISO\ 639-2\ codes.txt
	share/doc/SceneGenerators
	share/doc/ipmpx_syntax.bt
	Changelog
	README.md
)

my_use() {
	local flag="$1" pflag="${2:-$1}"
	if use ${flag}; then
		echo "--use-${pflag}=system"
	else
		echo "--use-${pflag}=no"
	fi
}

src_prepare() {
	default
	sed -i -e "s:\(--disable-.*\)=\*):\1):" configure || die
	# TODO: remove when old zlib is no longer in tree
	has_version "<sys-libs/zlib-1.3" && eapply "${FILESDIR}/${PN}-1.0.0-zlib-compile.patch"
}

src_configure() {
	tc-export CC CXX AR RANLIB

	local myeconfargs=(
		--cc="$(tc-getCC)"
		--libdir="$(get_libdir)"
		--verbose
		--enable-pic
		--enable-svg
		--use-ogg=system
		$(use_enable dvb dvb4linux)
		$(use_enable jack jack yes)
		$(use_enable opengl 3d)
		$(use_enable pulseaudio pulseaudio yes)
		$(my_use a52)
		$(my_use aac faad)
		$(use_enable dvb dvbx)
		$(my_use ffmpeg)
		$(my_use jpeg)
		$(my_use jpeg2k openjpeg)
		$(my_use mad)
		$(my_use png)
		$(my_use theora)
		$(my_use truetype freetype)
		$(my_use vorbis)
		$(my_use xvid)
	)

	if !use X ; then
		myeconfargs+=(
			--disable-x11
			--disable-x11-shm
			--disable-x11-xv
		)
	fi

	if use amd64 || use x86 ; then
		# Don't pass -mno-sse2 on non amd64/x86
		myeconfargs+=(
			--extra-cflags="${CFLAGS} $(usex cpu_flags_x86_sse2 '-msse2' '-mno-sse2')"
		)
	else
		myeconfargs+=(
			--extra-cflags="${CFLAGS}"
		)
	fi

	econf "${myeconfargs[@]}"
}

src_install() {
	einstalldocs
	emake STRIP="true" DESTDIR="${ED}" install
	emake STRIP="true" DESTDIR="${ED}" install-lib
}
