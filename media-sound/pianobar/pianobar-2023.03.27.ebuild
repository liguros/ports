# Copyright 2022-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit toolchain-funcs flag-o-matic

HOMEPAGE="https://6xq.net/pianobar/"
DESCRIPTION="A console-based replacement for Pandora's flash player"
GIT_COMMIT="218f3f84965e648793bcb2b0b9d97de636de01e7"
SRC_URI="https://github.com/PromyLOPh/pianobar/archive/${GIT_COMMIT}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 x86"
LICENSE="MIT"
SLOT="0"
IUSE=""

BDEPEND="virtual/pkgconfig"
RDEPEND="
	dev-libs/json-c:=
	dev-libs/libgcrypt:0=
	media-libs/libao
	>=media-video/ffmpeg-3.3:0=
	net-misc/curl
"
DEPEND="${RDEPEND}"
S=${WORKDIR}/${PN}-${GIT_COMMIT}

src_compile() {
	append-cflags -std=c99
	tc-export AR CC
	emake V=1 DYNLINK=1
}

src_install() {
	emake DESTDIR="${D}" PREFIX=/usr LIBDIR=/usr/$(get_libdir) DYNLINK=1 install
	dodoc ChangeLog README.rst

	rm "${D}"/usr/lib*/*.a || die

	docinto contrib
	dodoc -r contrib/{config-example,*.sh,eventcmd-examples}
	docompress -x /usr/share/doc/${PF}/contrib
}
