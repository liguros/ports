# Copyright 2025 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake-multilib

SRC_URI="https://github.com/ngtcp2/ngtcp2/releases/download/v${PV}/${P}.tar.xz"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~loong ~mips ~ppc ~ppc64 ~riscv ~sparc ~x86"

DESCRIPTION="Implementation of the IETF QUIC Protocol"
HOMEPAGE="https://github.com/ngtcp2/ngtcp2/"

LICENSE="MIT"
SLOT="0/0"
IUSE="gnutls libressl openssl +ssl static-libs test"
# Without static-libs, src_test just won't run any tests and "pass".
REQUIRED_USE="ssl? ( || ( gnutls libressl openssl ) ) test? ( static-libs )"

BDEPEND="virtual/pkgconfig"
RDEPEND="
	ssl? (
		gnutls? ( >=net-libs/gnutls-3.7.2:=[${MULTILIB_USEDEP}] )
		openssl? ( >=dev-libs/openssl-1.1.1:=[${MULTILIB_USEDEP}] )
		libressl? ( dev-libs/libressl:=[${MULTILIB_USEDEP}] )
	)
"
DEPEND="
	${RDEPEND}
	test? ( >=dev-util/cunit-2.1[${MULTILIB_USEDEP}] )
"
RESTRICT="!test? ( test )"

multilib_src_configure() {
	local mycmakeargs=(
		-DENABLE_STATIC_LIB=$(usex static-libs)
		-DENABLE_GNUTLS=$(usex gnutls)
		-DENABLE_BORINGSSL=OFF
		-DENABLE_PICOTLS=OFF
		-DENABLE_WOLFSSL=OFF
		-DCMAKE_DISABLE_FIND_PACKAGE_Libev=ON
		-DCMAKE_DISABLE_FIND_PACKAGE_Libnghttp3=ON
		-DBUILD_TESTING=$(usex test)
	)

	if use libressl || use openssl; then
		 mycmakeargs+=( -DENABLE_OPENSSL=ON )
	else
		 mycmakeargs+=( -DENABLE_OPENSSL=OFF )
	fi

	cmake_src_configure
}

multilib_src_test() {
	cmake_build check
}
