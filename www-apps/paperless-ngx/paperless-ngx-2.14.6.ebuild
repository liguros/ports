# Copyright 2023-2025 LiGurOS Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
inherit systemd tmpfiles

DESCRIPTION="Community supported paperless: scan, index and archive your physical documents"
HOMEPAGE="https://github.com/paperless-ngx/paperless-ngx"
SRC_URI="https://github.com/paperless-ngx/paperless-ngx/releases/download/v${PV}/paperless-ngx-v${PV}.tar.xz"
S="${WORKDIR}/${PN}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="audit compression mysql +ocr postgres remote-redis +sqlite zxing"
REQUIRED_USE="|| ( mysql postgres sqlite )"

ACCT_DEPEND="
	acct-group/paperless
	acct-user/paperless
"
EXTRA_DEPEND="
	app-text/unpaper
	dev-python/hiredis
	dev-python/websockets
"

DEPEND="
	${ACCT_DEPEND}
	${EXTRA_DEPEND}
	dev-python/asgiref
	dev-python/bleach
	dev-python/celery
	>=dev-python/channels-4.1
	>=dev-python/channels-redis-4.0
	dev-python/concurrent-log-handler
	>=dev-python/dateparser-1.2
	>=dev-python/django-5.1.1
	<dev-python/django-5.2
	dev-python/django-allauth
	dev-python/django-celery-results
	dev-python/django-cors-headers
	dev-python/django-extensions
	>=dev-python/django-filter-24.3
	dev-python/django-guardian
	dev-python/django-multiselectfield
	dev-python/django-redis
	dev-python/django-soft-delete
	>=dev-python/djangorestframework-3.15.2
	dev-python/django-rest-framework-guardian2
	dev-python/drf-writable-nested
	dev-python/filelock
	dev-python/httpx-oauth
	dev-python/imap-tools
	>=dev-python/inotifyrecursive-0.3
	>=dev-python/jinja2-3.1
	dev-python/langdetect
	dev-python/nltk
	dev-python/pathvalidate
	dev-python/pdf2image
	dev-python/pikepdf
	dev-python/pillow
	dev-python/python-dateutil
	dev-python/python-dotenv
	dev-python/python-gnupg
	>=dev-python/python-ipware-2.0.0
	dev-python/python-magic
	dev-python/pyzbar
	dev-python/rapidfuzz
	dev-python/redis
	>=dev-python/scikit-learn-1.5
	dev-python/tqdm
	>=dev-python/uvicorn-0.26.0
	>=dev-python/watchdog-4.0
	>=dev-python/whitenoise-6.8
	>=dev-python/whoosh-2.7
	media-gfx/imagemagick[xml]
	media-gfx/optipng
	media-libs/jbig2enc
	www-servers/gunicorn
	audit? ( dev-python/django-auditlog )
	compression? ( dev-python/django-compression-middleware )
	mysql? ( dev-python/mysqlclient )
	ocr? ( >=app-text/OCRmyPDF-16.5 )
	postgres? ( dev-python/psycopg:2 )
	!remote-redis? ( dev-db/redis )
	zxing? ( media-libs/zxing-cpp[python] )
"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/uvicorn-0.26.patch"
)

DOCS=( docker/imagemagick-policy.xml )

src_prepare() {
	default

	sed \
		-e "s|#PAPERLESS_CONSUMPTION_DIR=../consume|PAPERLESS_CONSUMPTION_DIR=/var/lib/paperless/consume|" \
		-e "s|#PAPERLESS_DATA_DIR=../data|PAPERLESS_DATA_DIR=/var/lib/paperless/data|" \
		-e "s|#PAPERLESS_MEDIA_ROOT=../media|PAPERLESS_MEDIA_ROOT=/var/lib/paperless/media|" \
		-e "s|#PAPERLESS_STATICDIR=../static|PAPERLESS_STATICDIR=/usr/share/paperless/static|" \
		-e "s|#PAPERLESS_CONVERT_TMPDIR=/var/tmp/paperless|PAPERLESS_CONVERT_TMPDIR=/var/lib/paperless/tmp|" \
		-i "paperless.conf" || die "Cannot update paperless.conf"

	cat >> "paperless.conf" <<- EOF

	# Custom
	#PAPERLESS_BIND_ADDR=unix
	#PAPERLESS_PORT=/run/paperless.sock

	PAPERLESS_ENABLE_COMPRESSION=$(use compression && echo true || echo false)
	PAPERLESS_AUDIT_LOG_ENABLED=$(use audit && echo true || echo false)
	EOF
}

src_install() {
	einstalldocs

	# Install service files
	newinitd "${FILESDIR}"/paperless-consumer.initd paperless-consumer
	newinitd "${FILESDIR}"/paperless-scheduler.initd paperless-scheduler
	newinitd "${FILESDIR}"/paperless-task-queue.initd paperless-task-queue
	newinitd "${FILESDIR}"/paperless-webserver.initd paperless-webserver

	systemd_newunit "${FILESDIR}"/paperless-webserver.service paperless-webserver.service
	systemd_newunit "${FILESDIR}"/paperless-webserver.socket paperless-webserver.socket
	systemd_newunit "${FILESDIR}"/paperless-scheduler.service paperless-scheduler.service
	systemd_newunit "${FILESDIR}"/paperless-consumer.service paperless-consumer.service
	systemd_newunit "${FILESDIR}"/paperless-task-queue.service paperless-task-queue.service
	systemd_newunit "${FILESDIR}"/paperless.target paperless.target
	if use remote-redis; then
		sed -e '/redis\.service/d' -i *.service "${D}$(systemd_get_systemunitdir)"/*.service
	fi

	# Install paperless files
	insinto /usr/share/paperless
	doins -r docs src static gunicorn.conf.py requirements.txt

	insinto /etc
	doins paperless.conf
	fowners root:paperless /etc/paperless.conf
	fperms 640 /etc/paperless.conf

	newtmpfiles "${FILESDIR}"/paperless.tmpfiles paperless.conf

	# Set directories
	for dir in consume data media tmp; do
		keepdir /var/lib/paperless/${dir}
		fowners paperless:paperless /var/lib/paperless/${dir}
		case "${dir}" in
		data) fperms 700 /var/lib/paperless/${dir} ;;
		*)    fperms 750 /var/lib/paperless/${dir} ;;
		esac
	done

	# Main executable
	fperms 755 "/usr/share/paperless/src/manage.py"
	dosym -r "/usr/share/paperless/src/manage.py" "/usr/bin/paperless-manage"
}

pkg_postinst() {
	tmpfiles_process paperless.conf
	elog "To complete the installation of paperless, edit /etc/paperless.conf file and"
	elog "* Create the database with"
	elog ""
	elog "sudo -u paperless paperless-manage migrate"
	elog ""
	elog "* Create a super user account with"
	elog ""
	elog "sudo -u paperless paperless-manage createsuperuser"
	elog ""
	elog "After each update of paperless, you should run migration with"
	elog ""
	elog "sudo -u paperless paperless-manage migrate"
	elog ""
	elog "Paperless services can be started together with"
	elog ""
	elog "sudo systemctl start paperless.target"
}
