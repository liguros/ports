# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="MPI library for algorithmic differentiation"
HOMEPAGE="
	https://www.mcs.anl.gov/~utke/AdjoinableMPI/AdjoinableMPIDox/index.html
	https://github.com/bremond/adjoinablempi
"
SRC_URI="https://gitlab.com/liguros/distfiles/-/raw/main/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 arm ~arm64 ~hppa ~ia64 ~ppc ppc64 ~riscv ~sparc x86"

RDEPEND="virtual/mpi"
DEPEND="${RDEPEND}"
S=${WORKDIR}/adjoinablempi-master

src_prepare() {
	default
	eautoreconf
}
