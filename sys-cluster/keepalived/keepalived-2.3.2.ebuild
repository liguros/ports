# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools systemd

DESCRIPTION="A strong & robust keepalive facility to the Linux Virtual Server project"
HOMEPAGE="https://www.keepalived.org/"
SRC_URI="https://www.keepalived.org/software/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sparc ~x86"
IUSE="+bfd dbus json libressl regex snmp systemd"

RDEPEND="dev-libs/libnl:=
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )
	dev-libs/popt
	net-libs/libnfnetlink
	sys-apps/iproute2
	regex? ( >=dev-libs/libpcre2-8:= )
	dbus? (
		sys-apps/dbus
		dev-libs/glib:2
	)
	json? ( dev-libs/json-c:= )
	snmp? ( net-analyzer/net-snmp:= )
	systemd? ( sys-apps/systemd )"
DEPEND="${RDEPEND}
	>=sys-kernel/linux-headers-4.4"

DOCS=(
	README CONTRIBUTORS INSTALL ChangeLog AUTHOR TODO
	doc/keepalived.conf.SYNOPSIS doc/NOTE_vrrp_vmac.txt
)

src_prepare() {
	default
	eapply ${FILESDIR}/SSL_set0_wbio.patch

	eautoreconf
}

src_configure() {
	econf \
		--with-init="$(usex systemd systemd custom)" \
		--with-kernel-dir="${ESYSROOT}"/usr \
		--enable-vrrp \
		$(use_enable bfd) \
		$(use_enable dbus) \
		$(use_enable json) \
		$(use_enable regex) \
		$(use_enable snmp) \
		$(use_enable snmp snmp-rfc) \
		$(use_enable systemd)
}

src_install() {
	default

	newinitd "${FILESDIR}"/keepalived.init-r1 keepalived
	newconfd "${FILESDIR}"/keepalived.confd-r1 keepalived

	systemd_newunit "${FILESDIR}"/${PN}.service-r1 ${PN}.service
	systemd_install_serviced "${FILESDIR}/${PN}.service.conf"

	use snmp && dodoc doc/*MIB.txt

	# This was badly named by upstream, it's more HOWTO than anything else.
	newdoc INSTALL INSTALL+HOWTO
	# Clean up sysvinit files
	rm -rv "${ED}"/etc/sysconfig || die
}
