# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
inherit autotools

DESCRIPTION="A utility for finding, fingerprinting and testing IKE VPN servers"
HOMEPAGE="https://github.com/royhills/ike-scan/"
SRC_URI="https://github.com/royhills/ike-scan/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc x86"
IUSE="libressl ssl"

DEPEND="
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)
"
RDEPEND="
	${DEPEND}
"

PATCHES=(
	"${FILESDIR}"/${PN}-1.9.5-clang-16.patch
	"${FILESDIR}"/${PN}-1.9.5-openssl-libdir.patch
)

src_prepare() {
	# Fix buffer overflow, bug #277556
	sed \
		-e "/MAXLINE/s:255:511:g" \
		-i ike-scan.h || die

	default

	eautoreconf
}

src_configure() {
	econf $(use_with ssl openssl)
}

src_install() {
	default
	dodoc udp-backoff-fingerprinting-paper.txt
}
