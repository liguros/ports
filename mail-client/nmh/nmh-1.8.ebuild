# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="New MH mail reader"
HOMEPAGE="http://www.nongnu.org/nmh/"
SRC_URI="https://download-mirror.savannah.gnu.org/releases/nmh/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux"
IUSE="gdbm libressl readline sasl ssl"

DEPEND="
	gdbm? ( sys-libs/gdbm )
	!gdbm? ( sys-libs/db:= )
	>=sys-libs/ncurses-5.2:0=
	net-libs/liblockfile
	readline? ( sys-libs/readline:0= )
	sasl? ( dev-libs/cyrus-sasl )
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)
	!!media-gfx/pixie
	!!sys-apps/pick
	!!sci-mathematics/snns
"
RDEPEND="
	${DEPEND}
	virtual/editor
	virtual/pager
"

DOCS=( DATE MACHINES README )

PATCHES=(
	"${FILESDIR}"/${PN}-1.3-db5.patch
)

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	local myconf=(
		--prefix="${EPREFIX}"/usr
		--libdir="${EPREFIX}"/usr/$(get_libdir)/nmh
		--mandir="${EPREFIX}"/usr/share/man
		--sysconfdir="${EPREFIX}"/etc/nmh
		$(use_with sasl cyrus-sasl)
		$(use_with ssl tls)
		$(use_with readline)
	)

	# have gdbm use flag actually control which version of db in use
	if use gdbm; then
		myconf+=( --with-ndbmheader=gdbm/ndbm.h --with-ndbm=gdbm_compat )
	else
		if has_version ">=sys-libs/db-2"; then
			myconf+=( --with-ndbmheader=db.h --with-ndbm=db )
		else
			myconf+=( --with-ndbmheader=db1/ndbm.h --with-ndbm=db1 )
		fi
	fi

	econf "${myconf[@]}"
}
