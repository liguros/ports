# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit rebar

DESCRIPTION="TLS/SSL native driver for Erlang and Elixir"
HOMEPAGE="https://github.com/processone/fast_tls"
SRC_URI="https://github.com/processone/fast_tls/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm ~ia64 ~ppc ~sparc ~x86"
IUSE="libressl"

DEPEND="
	>=dev-erlang/p1_utils-1.0.25
	>=dev-erlang/p1_utils-1.0.26
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )"
RDEPEND="${DEPEND}"

DOCS=( CHANGELOG.md README.md )

PATCHES=( $FILESDIR/rebar_config.patch )

src_prepare() {
	if use libressl; then
		eapply -p1 ${FILESDIR}/SSL_get_extms_support.patch
	fi

	default
}
