# Copyright 2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit acct-user

DESCRIPTION="ProxySQL user"

ACCT_USER_ID=-1
ACCT_USER_HOME_OWNER=proxysql:proxysql
ACCT_USER_HOME_PERMS=750
ACCT_USER_SHELL=/bin/false
ACCT_USER_GROUPS=( proxysql )

acct-user_add_deps
