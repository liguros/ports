# Copyright 2020-2023 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Golang rewrite of ego"
HOMEPAGE="https://gitlab.com/liguros/ego"
SRC_URI="https://gitlab.com/liguros/ego/-/archive/${PV}/${P}.tar.gz"

inherit go-module

LICENSE="Apache-2.0 BSD BSD-2 ISC MIT MPL-2.0"
SLOT="0"
KEYWORDS="*"

BDEPEND="dev-lang/go"

DOCS=( README.md )
PROG_VERS=${PV}

program_make() {
	local my_tags=(
		prod
	)
	local my_makeopt=(
		TAGS="${my_tags[@]}"
		LDFLAGS="-extldflags \"${LDFLAGS}\""
	)
	export MDATE=`date -u +%Y%m%d.%H%M%S`
	export PROG_VERS="${PROG_VERS}"
	emake "${my_makeopt[@]}" "$@"
}

src_compile() {
	program_make build
}

src_install() {
	# Install binary
	dobin ${PN}

	# Install docs
	einstalldocs

	# Install config file
	insinto /etc/${PN}
	doins etc/${PN}.yml
	doins etc/liguros.conf.example
	doins etc/parent.example
}
