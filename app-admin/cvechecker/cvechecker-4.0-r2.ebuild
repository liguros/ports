# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit autotools

DESCRIPTION="Tool to match installed software against the list of CVE entries"
HOMEPAGE="https://github.com/sjvermeu/cvechecker/wiki"
SRC_URI="https://github.com/sjvermeu/${PN}/archive/refs/tags/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sqlite mysql"

DEPEND="
	sqlite? ( >=dev-db/sqlite-3.6.23.1 )
	mysql? ( >=dev-db/mysql-5.1.51:* )
	dev-libs/libbsd
	>=dev-libs/libconfig-1.3.2
	acct-group/cvechecker
"
RDEPEND="
	${DEPEND}
	>=dev-libs/libxslt-1.1.26
	>=app-misc/jq-1.6-r3
"

S="${WORKDIR}/${PN}-${P}"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf \
		$(use_enable sqlite sqlite3) \
		$(use_enable mysql) || die "./configure failed"
}

src_compile() {
	emake || die "compile failed"
}

src_install() {
	emake DESTDIR="${D}" install || die
	keepdir "/var/lib/cvechecker/cache"
	keepdir "/var/lib/cvechecker/local"

	emake DESTDIR="${D}" postinstall || die
}
