help([[
Set emacs as your default editor.
]])

setenv("EDITOR", "/usr/bin/emacs")
